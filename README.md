# La Pension Radio - WIP

La Pension Radio is a intended to be a radio. For now it is only something like a playlist as the live option is not implemented yet

Link: https://priceless-fermat-ac9333.netlify.app/

## Features

- Play a set of songs

## Tech

- [reactjs]
- [typescript]
- [styled-components]
- [node.js] - netlify functions for the backend

## Installation
```sh
cd dillinger
npm i
npm build
npm start
```