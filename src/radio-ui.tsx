import React, { useEffect, useRef, useState } from 'react';
import { FaPlay, FaPause, FaAngleDoubleLeft, FaAngleDoubleRight } from 'react-icons/fa';
import styled from 'styled-components';

import { Button } from './components';
import { showElement, hideElement } from './utils';

const UIContainer = styled.div`
    width: 100vw;
    height: 95vh;
    margin: 0;
    padding: 0;

    position: absolute;
    top: 0;
    z-index: 5;

    display: flex;
    align-items: flex-end;
    justify-content: center;

    overflow: hidden;
`;

const RadioButton = styled (Button)`
    margin-left: 1.5vw;
    margin-right: 1.5vw;

    font-size: calc(16px + 2.5vw);
`;

const SongInfo = styled.div`
    width: 50vw;
    position: absolute;
    bottom: 40vh;
    right: 25vw;

    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
`;

const Title = styled.div`
    width: 100%;

    font-size: calc(16px + 4vw);

    color: var(--primary-color);

    text-align: center;
`;

const Actors = styled.div`
    width: 100%;
    margin-top: 1vh;

    font-size: calc(16px + 1vw);

    color: var(--primary-color);

    text-align: center;
`;

/*
const Header = styled.div`
    width: 100%;
    height: 15%;

    position: absolute;
    top: 0;

    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: center;
`;

const HeaderTitle = styled.div`
    color: var(--primary-color);
    font-size: calc(16px + 2vw);
`;
*/

function RadioUI(props : any) {
    const [mounted, setMounted] = useState(false);

    const timeoutRef = useRef<number | null> (null);

    const UIref = useRef<HTMLDivElement>(null);
    const propRef = useRef<Boolean>(props.isPlaying);
    const clipRef = useRef<Boolean>(props.hasClip);

    useEffect (() => {
        propRef.current = props.isPlaying;
        clipRef.current = props.hasClip;
        if (!mounted) {
            document.addEventListener("mousemove", (event : MouseEvent) => {
                if (timeoutRef.current) {
                    clearTimeout (timeoutRef.current);
                }
                showElement (UIref.current);
                timeoutRef.current = setTimeout (() => {
                    if (propRef.current && clipRef.current)
                        hideElement (UIref.current);
                }, 3000);
            });
            setMounted(true);
        }
    }, [mounted, propRef, clipRef, props, UIref, timeoutRef]);

    return (
        <UIContainer ref={UIref}>
            <RadioButton onClick={() => { props.playPrev() }}> 
                <FaAngleDoubleLeft />
            </RadioButton>
            <RadioButton onClick={() => { props.togglePlay() }}>
                { props.isPlaying ? <FaPause /> : <FaPlay />}
            </RadioButton>
            <RadioButton onClick={() => { props.playNext() }}>
                <FaAngleDoubleRight />
            </RadioButton>
            <SongInfo >
                <Title onFocus={(e) => e.currentTarget.blur()}>{ props.title }</Title>
                <Actors onFocus={(e) => e.currentTarget.blur()}>{ props.actors }</Actors>
            </SongInfo>
        </UIContainer>
    );
}

export default RadioUI;