import styled from 'styled-components';

const Button = styled.button`
    border: none;
    background: none;
    color: var(--primary-color);
`;

export { Button };