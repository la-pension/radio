import React, { useState, useRef, useEffect } from 'react';
import styled from 'styled-components';
import ReactPlayer from 'react-player';
import axios from 'axios';
import { writeStorage, useLocalStorage } from '@rehooks/local-storage';

import RadioUI from './radio-ui';
import { shuffle } from './utils';

//import { ReactComponent as PensionLogo } from './img/logo3.svg';

const ENDING_THRESHOLD = 2;

const getArtistNames = (song : any) => {
    if (song === undefined || song === null)
        return "";

    let names = song.artists[0];
    for (let i = 1; i < song.artists.length; i++) {
        names += " X " + song.artists[i];
    }
    return names;
}

const getTitle = (song : any) => {
    if (song === undefined || song === null)
        return "";
    return song.title;
}

const getUrl = (song : any) => {
    if (song === undefined || song === null)
        return "";
    return song.url;
}

const getHasClip = (song : any) => {
    if (song === undefined || song === null)
        return false;
    return song.hasClip;
}

const getIsPlaying = (isplaying : boolean) => {
    return isplaying;
}

const RadioContainer = styled.div`
    height: 100vh;
    width: 100vw;

    margin: 0;
    padding: 0;

    transform: scale(1.2);

    pointer-events: none;

    overflow: hidden;
`;

const Panel = styled.div`
    width: 100vw;
    height: 100vh;

    margin: 0;
    padding: 0;

    overflow: hidden;

    position: absolute;
    top: 0;
    z-index: 2;

    background: black;
`;

/*
const Logo = styled (PensionLogo as StyledComponent<"svg", DefaultTheme, {}, never>)`
    width: 20vw;
    height: auto;

    position: absolute;
    top: 35vh;
    left: 40vw;

    stroke: white;
    fill: white;
    @keyframes dash {
        from {
            stroke-dashoffset: 1000;
        }
        to {
            stroke-dashoffset: 0;
        }
    }
    stroke-dasharray: 1000;
    stroke-dashoffset: 1000;
    animation: 1s dash infinite linear alternate;
`;
*/

function Radio() {
    const panelRef = useRef<HTMLDivElement>(null);
    const playerRef = useRef<ReactPlayer>(null);
    const logoRef = useRef<SVGSVGElement>(null);
    const [ isPlaying, setIsPlaying ] = useState(false);
    const [ songs, setSongs ] = useState<any[]>([]);
    const [ gotSongs, setGotSongs ] = useState(false);

    const [ song, setSong ] = useState(0);
    const [songIsEndingId, setSongIsEndingId] = useState(-1);
    const [buffering] = useLocalStorage<boolean>('buffering', false);
    const [panelTimeoutId] = useLocalStorage<number>('panelTId', -1);

    const hidePanel = (delay : number) => {
        if (panelTimeoutId !== -1) {
            return;
        }

        writeStorage('panelTId', setTimeout(() => {
            if (songs[song] && songs[song].hasClip) {
                panelRef.current?.classList.add('hidden');
                logoRef.current?.classList.add('hidden')
            }

            writeStorage('buffering', false);
            writeStorage ('panelTId',-1);
        }, delay));
    }

    const showPanel = (delay : number) => {
        if (panelTimeoutId !== -1) {
            clearTimeout(panelTimeoutId);
            writeStorage('panelTId', -1);
        }

        setTimeout(() => {
            if (panelRef.current?.classList.contains('hidden')) {
                panelRef.current?.classList.remove('hidden');
                logoRef.current?.classList.remove('hidden');
            }
        }, delay);
    }

    const songIsEnding = () => {
        if (playerRef.current === null
            || playerRef.current.getDuration() === null
            || !songs[song])
            return;

        if (songs[song].endTime < 0) {
            if (playerRef.current.getDuration() - playerRef.current.getCurrentTime()
                <= ENDING_THRESHOLD)
                showPanel(0)
        } else {
            if (playerRef.current.getCurrentTime()
                >= songs[song].endTime)
                showPanel(0)
        }
    }

    const onPlay = () => {
        hidePanel(1000);
    }

    const onPause = () => {
        showPanel(0);
        setIsPlaying(false);
    }

    const onBuffer = () => {
        writeStorage('buffering', true);
        showPanel(0);
        if (songIsEndingId !== -1)
            clearInterval(songIsEndingId);
        setSongIsEndingId(setInterval(songIsEnding, 1));
    }

    const onBufferEnd = () => {
        hidePanel(1000);
    }

    const onEnded = () => {
        if (playerRef.current !== null && songs.length === 1) {
            playerRef.current.seekTo(0.0, "seconds");
        } else {
            setSong(song + 1 >= songs.length ? 0 : song + 1);
        }
    }

    const getSongs = () => {
        axios.get('https://dazzling-poincare-eda12a.netlify.app/.netlify/functions/song-read-all')
        .then((res) => {
            let data = [];
            for (let elt of res.data) {
                data.push(elt.data);
            }
            let shuffled = shuffle(data);
            setSongs(shuffled);
        }); 
    } 

    const PlayPause = () => {
        if (!buffering && panelTimeoutId === -1)
            setIsPlaying (!isPlaying);
    }

    const Next = () => {
        if (buffering || panelTimeoutId !== -1)
            return;
        setSong(song + 1 >= songs.length ? 0 : song + 1);
        showPanel(0);
    }

    const Prev = () => {
        if (buffering || panelTimeoutId !== -1)
            return;
        setSong(song - 1 < 0 ? songs.length - 1 : song - 1);
        showPanel(0);
    }

    useEffect(() => {
        if (!gotSongs) {
            getSongs();
            setGotSongs(true);
        }
    }, [gotSongs]);

    return (
        <div>
            <RadioContainer>
                <ReactPlayer
                    ref={playerRef}
                    width={'100%'}
                    height={'100%'}
                    url={getUrl(songs[song])}
                    playing={isPlaying}
                    controls={false}
                    playsinline={true}
                    onPlay={onPlay}
                    onPause={onPause}
                    onBuffer={onBuffer}
                    onBufferEnd={onBufferEnd}
                    onEnded={onEnded}
                    config={{
                        youtube: {
                            playerVars: {
                                controls: 0,
                                disablekb: 1,
                                showinfo: 0,
                                fs: 0,
                                rel: 0,
                                modestbranding: 1,
                                iv_load_policy: 3,
                                autoplay: 0
                            },
                            embedOptions: {

                            },
                        }
                    }}
                />
            </RadioContainer>
            <Panel ref={panelRef}></Panel>
            <RadioUI
                togglePlay={() => PlayPause()}
                playNext={() => Next()}
                playPrev={() => Prev()}
                isPlaying={getIsPlaying(isPlaying)}
                hasClip={getHasClip(songs[song])}
                title={getTitle(songs[song])}
                actors={getArtistNames(songs[song])}/>
        </div>
    );
}

export default Radio;