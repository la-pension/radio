
function hideElement(elt : HTMLDivElement | null) {
    if (!elt || elt.classList.contains('hidden'))
        return;

    elt.classList.add('hidden');
}

function showElement(elt : HTMLDivElement | null) {
    if (!elt || !elt.classList.contains('hidden'))
        return;

    elt.classList.remove('hidden');
}

function shuffle(array : any[]) {
    let res = [];
    while (array.length > 0) {
        let i = Math.floor(Math.random() * array.length);
        res.push(array[i]);
        array.splice(i, 1);
    }
    return res;
}

export { hideElement, showElement, shuffle };